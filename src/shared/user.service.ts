import { EventEmitter } from '@angular/core';
import { User } from './user';

export class UserService {
  usersChange = new EventEmitter<User[]>();
  private users: User[] = [
    new User('Вася Пупкин', 'vasy@gmail.com', true, 'user'),
    new User('Иван Иванов', 'ivanushka@gmail.com', true, 'editor'),
    new User('Jhon Doe', 'jhon.doe@gmail.com', true, 'admin')
  ];

  getUsers() {
    return this.users.slice();
  }

  addUser(user: User) {
    this.users.push(user);
    this.usersChange.emit(this.users);
  }
}
