import { User } from './user';

export class Group {
  constructor(
    public nameGroup: string,
    public usersInGroup: User[],
    public selectedItem: boolean,
  ) {}
}
