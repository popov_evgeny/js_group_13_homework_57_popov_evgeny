import { EventEmitter } from '@angular/core';
import { Group } from './group';
import { User } from './user';


export class GroupService {
  groupsChange = new EventEmitter<Group[]>();
  nameGroup = '';
  changesInGroup = true;
  groupChangesSaved = true;

  private groups: Group[] = [
    new Group('Hiking group', [], false),
    new Group('IT group', [], false),
    new Group('Velo group', [], false),
  ];

  getGroup() {
    return this.groups.slice();
  }

  addGroup(group: Group) {
    this.groups.push(group);
    this.groupsChange.emit(this.groups);
  }

  addUsers(user: User) {
    if (this.changesInGroup) {
      const group = this.groups.find(group => group.nameGroup === this.nameGroup );
      if (group) {
        let userName = group.usersInGroup.find(group => group === user);
        if (!userName) {
          group.usersInGroup.push(user);
          this.groupsChange.emit(this.groups);
        }
      }
    }
  }
}
