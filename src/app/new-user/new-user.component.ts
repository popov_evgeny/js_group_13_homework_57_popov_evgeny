import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { UserService } from '../../shared/user.service';
import { User } from '../../shared/user';
import { Group } from '../../shared/group';
import { GroupService } from '../../shared/group.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent {
  @Input() checked = '';

  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('emailInput') emailInput!: ElementRef;
  @ViewChild('select') select!: ElementRef;
  @ViewChild('groupInput') groupInput!: ElementRef;

  active = false;
  blankFieldName = false;
  blankFieldEmail = false;
  blankFieldSelect = false;
  blankFieldGroup = false;

  constructor(private userService: UserService, private groupService: GroupService) {}

  addNewUser(event: Event) {
    event.preventDefault();
    const name = this.nameInput.nativeElement.value;
    const email = this.emailInput.nativeElement.value;
    const role = this.select.nativeElement.value;

    if (name !== '') {
      if (email !== '') {
        if (role !== 'Choose...') {
          const user = new User(name, email, this.active, role);
          this.userService.addUser(user);
          this.nameInput.nativeElement.value = '';
          this.emailInput.nativeElement.value = '';
          this.select.nativeElement.value = 'Choose...';
          this.checked = '';
        } else {
          alert('Please fill in all data...');
          this.paintAnEmptyInputField(1);
        }
      } else {
        alert('Please fill in all data...');
        this.paintAnEmptyInputField(2);
      }
    } else {
      alert('Please fill in all data...');
      this.paintAnEmptyInputField(3);
    }
  }

  getChecked() {
    if (this.checked === '') {
      this.checked = 'checked';
      this.active = true;
    } else {
      this.checked = '';
      this.active = false;
    }
  }

  paintAnEmptyInputField(number: number) {
    if (number === 3) {
      this.blankFieldName = true;
      this.blankFieldEmail = true;
      this.blankFieldSelect = true;
      setTimeout( () => {
        this.blankFieldName = false;
        this.blankFieldEmail = false;
        this.blankFieldSelect = false;
      }, 1500);
    } else if (number === 2) {
      this.blankFieldEmail = true;
      this.blankFieldSelect = true;
      setTimeout( () => {
        this.blankFieldEmail = false;
        this.blankFieldSelect = false;
      }, 1500);
    } else {
      this.blankFieldSelect = true;
      setTimeout( () => {
        this.blankFieldSelect = false;
      }, 1500);
    }
  }

  addGroup(event: Event) {
    event.stopPropagation();
    if (this.groupInput.nativeElement.value === ''){
      alert('Please fill in all data...');
      this.blankFieldGroup = true
      setTimeout( () => {
        this.blankFieldGroup = false;
      }, 1500);
    } else {
      const nameGroup = this.groupInput.nativeElement.value;
      const group = new Group(nameGroup, [], false);
      this.groupService.addGroup(group);
      this.groupInput.nativeElement.value = '';
    }
  }
}
