import { Component, Input, OnInit } from '@angular/core';
import { Group } from '../../shared/group';
import { GroupService } from '../../shared/group.service';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {
  @Input() index = 0;

  groups!: Group[];


  constructor(private groupService: GroupService) {}

  ngOnInit(): void {
    this.groups = this.groupService.getGroup();
    this.groupService.groupsChange.subscribe((groups: Group[]) => {
      this.groups = groups;
    });
  }

  onClickGroup(resultingGroupName: string) {
    if (this.groupService.groupChangesSaved){
      this.groupService.changesInGroup = true;
      this.groupService.nameGroup = resultingGroupName;
      for (let i = 0; i < this.groups.length; i++) {
        this.groups[i].selectedItem = this.groups[i].nameGroup === resultingGroupName;
      }
      this.groupService.groupChangesSaved = false;
    } else {
      alert ('Please save changes!');
    }
  }

  saveChangesInGroup(event: Event) {
    event.stopImmediatePropagation();
    for (let i = 0; i < this.groups.length; i++) {
      this.groups[i].selectedItem = false;
    }
    this.groupService.changesInGroup = false;
    this.groupService.groupChangesSaved = true;
  }
}
