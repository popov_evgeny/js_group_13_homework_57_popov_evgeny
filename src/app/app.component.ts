import { Component, OnInit } from '@angular/core';
import { UserService } from '../shared/user.service';
import { User } from '../shared/user';
import { Group } from '../shared/group';
import { GroupService } from '../shared/group.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  users!: User[];
  groups!: Group[];
  keyToOpenThePage = false;


  constructor(private userService: UserService, private groupService: GroupService) {}

  ngOnInit(): void {
    this.users = this.userService.getUsers();
    this.userService.usersChange.subscribe((users: User[]) => {
      this.users = users;
    });
    this.groups = this.groupService.getGroup();
    this.groupService.groupsChange.subscribe((groups: Group[]) => {
      this.groups = groups;
    });
  }

  onChangePage() {
    this.keyToOpenThePage = !this.keyToOpenThePage;
  }

}
