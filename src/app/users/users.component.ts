import { Component, Input, OnInit } from '@angular/core';
import { UserService } from '../../shared/user.service';
import { User } from '../../shared/user';
import { GroupService } from '../../shared/group.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  @Input() index = 0;
  @Input() active = '';
  users!: User[];

  constructor(private userService: UserService, private groupService: GroupService) {}

  ngOnInit(): void {
    this.users = this.userService.getUsers();
    this.userService.usersChange.subscribe((users: User[]) => {
      this.users = users;
    });
    this.getActive();
  }

  getActive() {
    for (let i = 0; i < this.users.length; i++) {
      if (this.users[i].active) {
        this.active = 'Active';
      } else {
        this.active = 'Not active';
      }
    }
  }

  getUser(user: User) {
    this.groupService.addUsers(user);
  }
}
